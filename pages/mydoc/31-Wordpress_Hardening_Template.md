---
title: Wordpress Hardening Template
keywords: email templates, WordPress, hardening, server
last_updated: August 12, 2021
tags: [infrastructure_templates, templates]
summary: "Email to Client with links on how to harden a Wordpress-based site"
sidebar: mydoc_sidebar
permalink: 31-Wordpress_Hardening_Template.html
folder: mydoc
conf: Public
lang: en
---


# Wordpress Hardening Template
## Email to Client with links on how to harden a Wordpress-based site

### Body

Hi {{ beneficiary name }},

I hope you are doing great.

I am happy to inform you that we are in a position to guide you through the steps you need to take to secure your WordPress website.

In order to avoid attacks from happening again, you can refer to the following WordPress Hardening recommendations: https://wordpress.org/support/article/hardening-wordpress/

The following articles can be considered as references as well:

- https://www.owasp.org/index.php/OWASP_Wordpress_Security_Implementation_Guideline

- https://www.circl.lu/pub/tr-26/

Keeping your CMS version and your plug-ins up to date is very important. But there are other security enhancements that can be implemented. Please don't hesitate to ask for our assistance.

All the best,

{{ incident handler name }}
