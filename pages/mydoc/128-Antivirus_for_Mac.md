---
title: Antivirus for Mac
keywords: Mac, antivirus
last_updated: February 17, 2023
tags: [devices_data_security, articles]
summary: "A client is asking for an antivirus recommendation on Mac. A client is a high risk person using a Mac computer."
sidebar: mydoc_sidebar
permalink: 128-Antivirus_for_Mac.html
folder: mydoc
conf: Public
lang: en
---


# Antivirus for Mac
## A client is asking for an antivirus recommendation for Mac

### Problem

Although the number of viruses targeting Mac OSX is lower than for Windows computers, this does not mean Mac users are not vulnerable to attacks.

In addition, although a system can be immune to certain viruses, it can still spread the infection within the user's network.

Therefore, it's important to use additional layers of protection, like an antivirus.


* * *


### Solution

When recommending an antivirus to a civil society actor, we should think of risk factors that might feature in the client's threat model. This can include for example, the country where the antivirus company is registered, their privacy policies, known alliances with companies or governments, etc.

When available, **first-party antivirus solutions are usually the strongest option**.

For MacOS, MalwareBytes is the recommended option. This [NY Times Wirecutter article](https://www.nytimes.com/wirecutter/blog/best-antivirus/) explains some of the reasoning behind this decision.

- [MalwareBytes](https://www.malwarebytes.com/mac-download/)

When providing assistance with the installation of an antivirus, we should send recommendations on some key points regarding antivirus software:

* Do not run more than one anti-virus program at the same time, as this might cause your computer to run extremely slowly or to crash. Uninstall one before installing the other.

* Make sure that your anti-virus program allows you to receive updates. Many commercial tools that come pre-installed on new computers must be registered (and paid for) at some point or they will stop receiving updates.

* Ensure that your anti-virus software updates itself regularly. New viruses are written and distributed every day, and your computer will quickly become vulnerable if you do not keep up with new virus definitions.

* Enable the "always on" virus-detection feature if your anti-virus software has one. Different tools have different names for it, but most of them offer a feature like this. It may be called 'Realtime Protection,' 'Resident Protection,' or something similar.

* Scan all the files on your computer regularly. You don't have to do this every day (especially if your anti-virus software has an "always on" feature, as described above) but you should do it from time to time. How often may depend on the circumstances. Have you connected your computer to unknown networks recently? With whom have you been sharing USB memory sticks? Do you frequently receive strange attachments by email? Has someone else in your home or office recently had virus problems?


* * *


### Comments


* * *

### Related Articles

- [Article #159: Security Measures for macOS Computers](159-Security_Mac.html)
- [Article #133: How to Clean a Malware-Infected Windows Machine](133-Clean_Malware_Windows.html)
- [Article #134: Removing Adware from a Windows Machine](134-Remove_Adware_Windows.html)