---
title: PCAP File Analysis with Wireshark to investigate Malware infection
keywords: Forensics, Vulnerabilities and Malware, System compromise
last_updated: March 11th, 2022
tags: [forensics, articles]
summary: "A device shows weird behavior or the user believes they opened a malicious file. The device is a smart phone, making forensics more difficult."
sidebar: mydoc_sidebar
permalink: 421-PCAP_Analysis_with_Wireshark.html
folder: mydoc
conf: Public
ref: PCAP_Analysis_with_Wireshark
lang: en
---



# PCAP File Analysis with Wireshark to investigate Malware infection

## How to analyze a PCAP file using Wireshark.

### Problem

A system is behaving strangely and you need to conduct a network perimeter analysis to check if it is compromised. Host-based investigation ([Article #367: Live Forensics for Windows](367-Live_Forensics_for_Windows.html) and [Article #368: Live Forensics for Linux](368-Live_Forensics_for_Linux.html)) has led to no result or it is not an option.

* * *


### Solution

#### Step 1: Traffic Capture

* Assist the beneficiary in creating and exporting a PCAP file capturing the traffic of the device that shows suspicious behavior. Capture the traffic for at least 2 hours and ideally for 24 hours as malware beacons can be done once daily.
   * Follow this [guide](https://www.howtogeek.com/104278/how-to-use-wireshark-to-capture-filter-and-inspect-packets/) for analysis on laptops.
   * Follow this [guide](https://www.wireshark.org/docs/wsug_html_chunked/AppToolstcpdump.html) on using tcpdump in command line if Wireshark is not an option.
   * Follow this [guide](https://guides.securitywithoutborders.org/guide-to-quick-forensics/smartphones/network.html) to capture a phone's traffic.
* Record the hash of the PCAP file on the beneficiary machine that was used to capture the traffic. Then when the PCAP file is received, check that the hash is the same. This step is needed in order to verify the integrity of the file. For more help, you can follow [this guide](https://technastic.com/check-md5-checksum-hash/) on how to verify the hash.
* The file can be sent over email if its size allows that. If not we can use a peer-to-peer file sharing channel such as [Onionshare](https://onionshare.org/). Other options can be found in [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html).

#### Step 2: Traffic Analysis using Wireshark

<a name="preparation"></a>
##### Preparing Wireshark

In order to make the analysis easier, make sure Wireshark is configured following this [tutorial](https://unit42.paloaltonetworks.com/using-wireshark-identifying-hosts-and-users/). This configuration will make the requested domains and connected hosts clearer to you. It will also change the time display to a readable version.


##### PCAP analysis

To load a PCAP file in Wireshark, open Wireshark and in the menu bar, click 'File', then click 'Open' and navigate to the file's location, then click 'Open.'

In our analysis of the PCAP file, we will try three analysis techniques to find any indicators of malicious activity. These steps can be performed in any order.

**Please escalate the case** if you run into any issues when following these steps, or if none of these reveal any results.

###### PCAP Analysis: Option 1

Following its executions, a malware usually attempts to connect to its command and control (C2) server. Most of the time one or more name resolutions take place. Investigate this using the instructions below and note the domains and the resolved IPs.

On the Menu Bar: `Statistics > Resolved Addresses`

Check any domain that seems unusual using the resources in [Article #140: Online Tools to Check a Websites' Reputation](140-websites_check_IP_reputation.html).

You may notice a domain is unusual if it was implicated in a previous incident or used to distribute previous malware and recorded in a MISP event, or if it is detected by [VirusTotal](https://www.virustotal.com/gui/). Or, you may suspect a domain may be newly registered, which could be an indicator that it was created to carry out a new attack.

###### PCAP Analysis: Option 2

Some malware connects directly to an IP without address resolution. This is also usually considered a malicious sign. Check for a direct IP address connection by following the instructions below and listing the IPs implicated. This will show the different endpoints in the captured traffic and the stats of exchanged traffic without needing to read the capture line by line:

On the Menu Bar: `Statistics > Conversations > IP4(in rare cases IP6)`

You can sort the endpoints by the volume of traffic. An IP that does not belong to a mainstream social media or service provider and that generates a considerable amount of traffic should be investigated using the resources in [Article #140: Online Tools to Check a Websites' Reputation](140-websites_check_IP_reputation.html). In order to find who the IP belongs to, you should check WHOIS records using a tool like [who.is](https://who.is/) or [ICANN Lookup](https://lookup.icann.org/). You can also use whois in terminal as follows: `whois url.org`

###### PCAP Analysis: Option 3

Domains and endpoints that the device connected to can be also checked using the 'Feeds' field of Wireshark. However, this field includes a lot of information that you won't need. In order to focus on the connected hosts and domains, apply the filter below to list all the requested hosts captured in the PCAP. To apply the filter, copy and paste the following text into the search bar, then press 'enter'.

`(http.request or tls.handshake.type eq 1 or tcp.flags eq 0x0002 or dns) and !(udp.port eq 1900)`

   - **NOTES:**
     - We add `and !(udp.port eq 1900)` to filter out device discovery traffic, which generates a lot of traffic that isn't necessary for this investigation.
     - Make sure you configured Wireshark based on the [Preparing Wireshark](#preparation) section above. If Wireshark is not configured accordingly, the results will be shown differently and the rest of the tips won't be applicable.
     - You can save this filter to use again in the future. With the filter text in the search bar, click on the plus ( + ) button, enter a title for the filter, and click 'OK'.


The results of this filter will include:
- domain resolution requests in the 'Info' section
- the host names of any http requests, including the associated IPs
- URLs requested in clear using http in the 'Info' section
- the server name of any TLS/https connections, including the associated IPs


In the situations where the traffic is not encrypted, we can analyze it using the exchanged data. For example, we can see if it uploaded or downloaded files or tried to connect to URLs. This can be done by choosing one line of the traffic that happens after the domain resolution, right clicking on that line, then selecting `Follow TCP Stream`. This will apply a filter on the stream which will only display the traffic of that connection, and show a raw presentation of the data exchanged between the C2 and the victim. Then you can export the exchanged data and conduct more analysis on it.

From such non encrypted traffic:
- If any file is found, export as following:

`File -> Export Objects -> HTTP`

 Then follow our malicious document analysis articles ([Article #394: MS Office Files static analysis](394-MS_Office_Files_static_analysis.html) and [Article #363: Analysing Suspicious PDF Files](363-Analysing_Suspicious_PDF_files.html)) to see if it is malicious or not, and to determine what next steps to take.  You can also use Cuckoo Sandbox.

- [urlscan.io](https://urlscan.io) can be used to analyze URLs without visiting them from your machine.

##### Post-Analysis Steps

- Prepare a report with the findings.

- Any malicious items found through analyzing the traffic can be used for further analysis:
   - We can check the browser cache and history of other devices on the same network as the infected device. This way we can see if other users visited or contacted the malicious domain or URLs.
   - We can check if any malicious file extracted from the traffic is present on other devices on the network, especially those that run the same operating system.
   - If a behavior analysis is available, using a sandbox for example, it should show how the execution of a downloaded malicious file would alter the system. Use that to check whether other devices have been altered in the same way.
   - The analysis of the downloaded file can help with the eradication and recovery phase.

- The analysis would inform us on what are the next steps that should be made to recover from the incidents.

- Indicators collected during such analysis should be saved on CiviCERT's MISP instance by following the instructions in [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html).


* * *


### Comments

You can practice this process on real traffic PCAPs using the exercises [here](https://www.malware-traffic-analysis.net/training-exercises.html).



* * *

### Related Articles

- [Article #140: Online Tools to Check a Website's Reputation](140-websites_check_IP_reputation.html)
- [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html)
- [Article #394: MS Office Files static analysis](394-MS_Office_Files_static_analysis.html)
- [Article #363: Analysing Suspicious PDF Files](363-Analysing_Suspicious_PDF_files.html)
- [Article #367: Live Forensics for Windows](367-Live_Forensics_for_Windows.html)
- [Article #368: Live Forensics for Linux](368-Live_Forensics_for_Linux.html)
