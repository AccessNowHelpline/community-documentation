---
title: Tails Boot Issues on Mac
keywords: Tails, known issues, macOS, Mac, boot failure
last_updated: September 30, 2019
tags: [devices_data_security, articles]
summary: "A Tails stick fails to boot on a Mac computer. The same Tails stick boots up normally on other machines."
sidebar: mydoc_sidebar
permalink: 127-Tails_Boot_Issues_Mac.html
folder: mydoc
conf: Public
ref: Tails_Boot_Issues_Mac
lang: en
---


# Tails Boot Issues on Mac
## Some solutions to deal with a functioning Tails stick that won't boot on a Mac computer

### Problem

A client owns a Mac computer and needs to run Tails on it, but Tails won't boot on their machine. They need to find a permanent solution for using Tails on their computer.


* * *


### Solution

The issue might be due to compatibility issues: the computer might not be suitable, Tails might need to be updated, or the storage device where Tails is installed might not be compatible with Mac.

*Please note that **any Mac with 32-bit EFI might not start on Tails**. You can check if a given Mac is 32-bit or 64-bit EFI in [this list](https://www.everymac.com/mac-answers/snow-leopard-mac-os-x-faq/mac-os-x-snow-leopard-64-bit-macs-64-bit-efi-boot-in-64-bit-mode.html).*

*Note: this article only lists solutions that have worked in a specific case, but may not work for other circumstances. It is recommended to consult the [Tails support page](https://tails.boum.org/support/index.en.html) and the [Tails Known Issues page](https://tails.boum.org/support/known_issues/index.en.html) for more information.


1. Get as much information about the Mac computer as possible. This is important because Tails has known issues on certain Mac models, which are listed [here](https://tails.boum.org/support/known_issues/index.en.html#index2h2) together with possible solutions.

2. Identify the version of Tails installed on the USB stick. Older versions might cause compatibility issues. Updating Tails to the latest version may resolve these issues (and fix other bugs).

    Upgrade instructions can be found [here](https://tails.boum.org/doc/upgrade/index.en.html).


3. If the Mac model is not listed in the known issues and the Tails installation is updated to the latest version and boots normally on other machines, the booting problem could be related to the USB brand or model. Trying other USB brands or models may help. It is also an option to boot from a CD or SD card, if either is available to the client.


* * *


### Comments


* * *

### Related Articles

