---
title: Initial Reply in English for a Vetted Beneficiary
keywords: email templates, initial reply, case handling policy, vetted
last_updated: March 1, 2023
tags: [helpline_procedures_templates, templates]
summary: "First response email to a beneficiary who has already been vetted."
sidebar: mydoc_sidebar
permalink: 448-Initial_Reply_English_Vetted.html
folder: mydoc
conf: Public
ref: Initial_Reply_English_Vetted
lang: en
---


# Initial Reply in English for Vetted Beneficiaries
## First response email to English-speaking beneficiary who has already been vetted


### Body


Dear {{ beneficiary name }},

Thank you for contacting the Access Now Digital Security Helpline (https://www.accessnow.org/help). My name is {{ incident handler name }} and I am here to help you.

We understand that our communications may be sensitive, so we encourage you to establish an end-to-end encrypted channel of communications. If you already use PGP for encrypted email, please send us your public key. You can find ours at https://keys.accessnow.org/help.asc. Otherwise please let us know if you use any other end-to-end encrypted communications channel, such as Signal messenger. You can follow this guide to install Signal: https://support.signal.org/hc/en-us/articles/360008216551-Installing-Signal

If you aren't familiar with these tools, we’ll be happy to guide you through setting one up so that we can communicate safely.

Take care,
{{ incident handler name }}

* * *

### Comments

* * *

### Related Articles

- [Article #225: FAQ - Initial Reply in Foreign Languages](225-FAQ-Initial_Replies_Multilanguage.html)
- [Article #17: Initial Reply in English](17-Initial_Reply.html)
