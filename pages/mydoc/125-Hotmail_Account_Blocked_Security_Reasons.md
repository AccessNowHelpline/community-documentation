---
title: Hotmail Account Blocked for Security Reasons
keywords: Hotmail, account security, account blocked, Microsoft
last_updated: September 30, 2019
tags: [account_security, articles]
summary: "A client has tried to change their Hotmail account settings and their account has been blocked for 30 days."
sidebar: mydoc_sidebar
permalink: 125-Hotmail_Account_Blocked_Security_Reasons.html
folder: mydoc
conf: Public
ref: Hotmail_Account_Blocked_Security_Reasons
lang: en
---


# Hotmail Account Blocked for Security Reasons
## Explanations for a client who cannot access their Hotmail account after they tried to change their settings

### Problem

When someone tries to change their password or their recovery email address or phone number, Hotmail asks to verify their identity by sending an email through their security info, like an alternate email address or phone number.

If the client doesn't have control of this security info (e.g. because it is incorrect or out of date), a notification will be displayed:

> Your 30-days waiting period ends on xx/xx/xxxx. Until then, you won't be able to sign in to your account unless you've found your security info.

The client is blocked and cannot complete the procedure for changing the password or the recovery email.


* * *


### Solution

Explain to the client that this is a normal notification from Hotmail. For security reasons, they need to verify your identity. If you lost your security info, like the recovery email or phone number, Hotmail requires a 30-day waiting period before allowing changes to the account.

More information on this process can be found [in the official troubleshooting guide](https://support.microsoft.com/en-us/help/12429/microsoft-account-cant-sign-in) (section "I changed my security info and have a 30-day waiting period"), and [in the page on Microsoft account security info](https://support.microsoft.com/en-us/help/12428/microsoft-account-security-info-verification-codes).


* * *


### Comments

