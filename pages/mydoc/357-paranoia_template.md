---
title: Initial Reply for Clients You Suspect Are Paranoid
keywords: helpline procedures templates, paranoia, psychological security, client in distress, initial reply
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response to clients who we suspect are suffering from paranoia"
sidebar: mydoc_sidebar
permalink: 357-paranoia_template.html
folder: mydoc
conf: Public
ref: paranoia_template
lang: en
---


# Initial Reply for Clients You Suspect Are Paranoid
## First response to clients who we suspect are suffering from paranoia

### Body

Dear {{ beneficiary name }},

Thank you for contacting the Access Now Digital Security Helpline (https://www.accessnow.org/help). My name is {{ incident handler name }}, and I am here to support you into understanding the problem that you are experiencing and to figure out what you want to do next.

We have received your initial request and I am working on it. To better support and respond to your needs, we need to ask a few questions and keep track of what has happened to follow up.

First, I am sorry you are going through this. I want to help as much as I can.  Before I am able to help, I need some more information.

Please take your time to answer these questions:

- Would it be appropriate if we set up a call with you, either video or just audio?
- Who do you think is trying to harm you?
- What is their motive?
- Why have they chosen you?
- Is there something you have that they need?
- What kind of threats or harm are occurring?

Before we talk again I want to ask you to help me out by keeping record of what happens, saving important information and find a trusted friend who can comfort you and give you a hand managing the incident you are experiencing.

Lastly, we would like to inform you that as part of our security measures, we vet new potential Helpline clients. This means we reach out to trusted contacts and share some information like your name, e-mail address and organization you belong to. I know that you are feeling discomfort right now and may not want opt in with this process. We would like to assure you that the reason why you are contacting us and any information related to you case well be kept strictly confidential and only available to the Helpline team. It will also be helpful to us if you can provide a reference person that you trust that can vouch for your identity. Information we need are at least two names and their contact details. If you so wish to not go through the vetting process, please let us know and we will try to find an alternative way.

Since the information we are exchanging may be sensitive and confidential we suggest that we establish a secure channel of communications. Let us know you if you happen to use secure communications channel like encrypted e-mail, Signal Messenger, Wire or XMPP/OTR. If you aren't familiar with any of these tools, we will be glad to guide you in setting up one of them to communicate with us securely.

Please continue to reply to this message including “[accessnow #{{ ticket id }}]” in the subject line and your answer to my questions

Kind regards and stay strong,
{{ incident handler name }}


* * *


### Comments

*You may have some of this information from the initial contact from the client.  If you already have it, repeat it back to them by writing, “I want to make sure I understand what is happening,” then repeat back the answers to the questions.*


* * *


### Related Articles

- [Article #356: Protocol for a Client We Suspect Is Paranoid](356-paranoia_protocol.html)
