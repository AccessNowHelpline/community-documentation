---
title: Project Management Tools for NGOs
last_updated: November 27, 2019
tags: [infrastructure, articles]
summary: "The Helpline has received a request for recommended NGO-friendly project management tools."
sidebar: mydoc_sidebar
permalink: 365-Project_Management_Tools.html
folder: mydoc
conf: public
ref: Project_Management_Tools
lang: en
---

# Project Management Tools for NGOs
## Recommendations on NGO-friendly project management tools

### Problem

A client has contacted us asking for a review or recommendations on a specific project management tools. We should help them identify the best tools based on their needs.


* * *


### Solution

#### Initial Questions

Sample questions you can ask the client to help them assess a project management tool:

1. Do they have the capacity to host the tool in their own network and in their own server? 

2. Do they need a project management tool that can integrate with other third-party applications (Google Apps, NextCloud, etc.)?

3. Do they need the tool to also run in mobile devices such as smartphones and tablets?

4. What are their security considerations?


#### Recommended tools

- [Framaboard](https://framaboard.org/)
    - Managed by the French non-profit organization for open source community software [Framasoft](https://framasoft.org/en/), Framaboard is a simple and open source visual task board manager inspired by the Kanban method.

- [Disroot Project Board](https://disroot.org/en/services/project-board) 
    - Managed by Dutch autonomous server [Disroot](https://disroot.org/). Disroot is a platform providing online services based on principles of freedom, privacy, federation and decentralization. Disroot's Project board is a project management tool powered by Taiga, working with agile methodology in mind.
    - [Features](https://board.disroot.org)
    - [Documentation](https://howto.disroot.org/)
  
- [Zimbra](https://www.zimbra.com)
    - Open source messaging and collaboration tool, offered by [Greenhost](https://greenhost.net/) among its managed services.
    - [Features](https://www.zimbra.com/email-server-software/product-edition-comparison/)
    - [Documentation](https://www.zimbra.com/documentation/)

- [Gitlab issue board](https://about.gitlab.com/product/issueboard/)
    - Gitlab is a well-known Git-repository manager providing wiki, issue-tracking, and also boards for project management.
    - [Features](https://docs.gitlab.com/ee/user/project/issue_board.html)
    - [Documentation](https://docs.gitlab.com/ee/user/project/issue_board.html)
 
- [Wekan](https://wekan.github.io/) 
    - Wekan is an open source Kanban board, it can be self-hosted, either on its own or as a [Sandstorm app](https://sandstorm.io/).
    - [Features](https://github.com/wekan/wekan/wiki/Features)
    - [Documentation](https://github.com/wekan/wekan/wiki)

- [Taiga](https://taiga.io/)
    - Taiga is an open source project management platform for agile developers & designers and project managers.
    - [Features](https://taiga.pm/)
    - [Documentation](https://taiga.pm/learn-about-taiga-2/)


* * *


### Comments

Greenhost, Virtualroad, and other NGO-friendly hosting providers listed  in [Article #88: Advice on Hosting](88-Advice_Hosting.html) would be able to host one of the above self-hosted services in a VPS.


