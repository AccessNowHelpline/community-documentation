---
title: Wordpress Hardening Template - Russian
keywords: email templates, WordPress, hardening, server, website protection
last_updated: August 12, 2021
tags: [infrastructure_templates, templates]
summary: "Письмо клиенту с советами по усилению защиты сайта на Wordpress"
sidebar: mydoc_sidebar
permalink: 308-Wordpress_Hardening_Template_ru.html
folder: mydoc
conf: Public
ref: Wordpress_Hardening_Template
lang: ru
---


# Wordpress Hardening Template
## Email to Client with links on how to harden a Wordpress-based site

### Body

Здравствуйте,

Надеюсь, у вас все хорошо.

Мне приятно сообщить, что мы готовы помочь вам с укреплением защиты вашего сайта на WordPress.

Чтобы избежать повторных атак, вы можете попробовать следующие рекомендации для владельцев сайтов на WordPress: https://codex.wordpress.org/Hardening_WordPress#Hardening_Recommendations

Эти статьи тоже могут оказаться полезными:

- https://github.com/ernw/hardening/blob/master/web_application/wordpress/4.4.2/ERNW_Wordpress_Hardening.md

- https://www.circl.lu/pub/tr-26/

Очень важно иметь самую свежую версию CMS и плагинов. Есть и другие способы повышения безопасности сайта. Пожалуйста, не стесняйтесь обращаться к нам за помощью.

С уважением,

{{ incident handler name }}
