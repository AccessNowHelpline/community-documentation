---
title: TLS and self-signed certificates
keywords: Infrastructure, website protection, website management, TLS, self-signed certificates, CA, Certificate Authority
last_updated: September 21, 2020
tags: [infrastructure, articles]
summary: "Knowledge base for TLS and self-signed certificates."
sidebar: mydoc_sidebar
permalink: 403-TLS_and_self-signed_certificates.html
folder: mydoc
conf: Public
ref: TLS_and_self-signed_certificates
lang: en
---


# TLS and self-signed certificates
## Knowledge base for TLS and self-signed certificates

### Problem

A client or Security Incident Handler requires information about the TLS Cryptosystem and its attributes, or about the best application of self-signed certificates in a system.

* * *

### Solution

The following information explains the nature of the TLS Cryptosystem. It also details the strengths, weaknesses, and use cases for self-signed certificates.

#### TLS Cryptosystem

It is important to remember that TLS is a trust oracle cryptosystem. Its design is based on the availability of third party trust oracles, known as Certificate Authorities, to verify the validity of the cryptography. This design works in most circumstances, but not all circumstances. Also, given that the number of CAs has grown beyond a certain critical mass, trust in the oracles has been eroded, as there are now plenty of CAs aligned with nation states, etc., that have ulterior motives and cannot be completely trusted, particularly by some users for whom the nation state is an adversary. This is the major flaw in using TLS for most of the web's encryption.    

Self-signing a TLS certificate *removes the reliance on a third party trust oracle*. This can be seen as bad (breaking the design of TLS), or good (providing a mechanism for TLS to work in situations where CAs are not trusted, or CAs are not available).      

Self-signed certificates for TLS should not be demonized, as they have their place. It is imperative, however, to understand what needs to be done to make self-signed certificates work, given the user context and the purpose of the encryption.      

There used to be another mechanism available within the major browsers to solve some of the situations in which the usual CAs could not be relied upon. That mechanism was HPKP (HTTP Public Key Pinning). Unfortunately HPKP has been deprecated, and is now removed from all the major browsers. However, this does not mean that the browsers are not implementing some form of static key pinning for particular certificates. Using some form of key pinning could allow for some certificates to be trusted without access to the CA, but as mentioned, the public standard for this has now been deprecated and it is unclear how it's currently implemented in different browsers.   

#### Self-signed Certificates

##### Strengths of Self-signed Certificates

- Work in a closed network where access to public CAs is not available      
- Provide increased control, including trust, over all aspects of the cryptosystem      
- Good for situations where encryption is needed, but only to defeat low-level threats, such as to remove the possibility of tracking mechanisms, etc., and where breaking the encryption with a man-in-the-middle attack would not be worthwhile to the adversary          

##### Weaknesses of Self-signed Certificates

- Key management takes more work, particularly the need to manually verify the certificate fingerprint      
- Manual configuration requires more effort than using public CAs          

##### Appropriate use of self-signed certificates

There are at least two appropriate uses of self-signed certificates in TLS.

The first is in situations that only require the lightest form of encryption. There may be more of these situations than you assume, and in such circumstances TLS with self-signed certificates is a good fit.

The second is in situations where circumstances dictate that self-signed certificates are the only easy and logical choice.    

**Lightweight encryption scenarios**        

There are many circumstances where strong encryption is not required. This includes cases where any form of encryption is enough to defeat the adversary, because that adversary is only motivated enough to consume data sent in the clear. This is often true, for example, for entities gathering data for the purpose of advertisement delivery. They are only interested in gathering data that is easily available, and do not want to risk the legal and other complications that may arise from defeating user encryption. TLS with self-signed certificates is a good fit here, since it is ubiquitous, free of charge, and can be set up without any delays that may otherwise be introduced by having to interact with a Certificate Authority.    

**Self-signed certificates as a logical choice**      

In situations where the network is closed in some way and access to an external third party trust oracle (CA) is not possible, self-signed certificates are the logical choice. Note that there is no inherent weakness in using self-signed certificates. However, since using a self-signed certificate removes the trust oracle from a trust oracle cryptosystem, it is necessary to verify the trust manually. To have confidence that no man-in-the-middle attack is being perpetrated, the user must *manually check the certificate fingerprint* of the certificate at the beginning of each browsing session. Doing so ensures the integrity of the encrypted connection, and restores the cryptosystem to a known safe state.

If the fingerprint is checked, and matches what is known to be the fingerprint for the self-signed certificate, then all is good, and the user can proceed knowing that the message content is encrypted. The difficult thing in operating this way is finding a reliable and trustworthy way of communicating the certificate fingerprint. Ideally this should be out-of-band to prevent a compromise of a single mechanism or piece of infrastructure from compromising both the certificate *and* the fingerprint. For this reason, proper key management for use of TLS with self-signed certificates is easiest where the user base is small, known, and where out-of-band communications are possible and already in place.        

* * *

### Comments

#### Verifying certificate fingerprints

For the users to have full confidence that the certificate they are about to import is correct, they should compare the certificate fingerprint before proceeding to import the certificate.

1. Administrators should publish the certificate fingerprint in a safe location (e.g. on an intranet site).
2. When the certificate has been downloaded, users should be instructed to "view" the certificate details first, and examine the fingerprint to verify that it matches the certificate fingerprint issued by their Administrator.
3. Once they have confirmed that the fingerprints match, users can proceed to import the certificate.

To manually verify the fingerprint after the certificate has been imported, the user may use one of the following methods:

- On Linux and MacOS, this command can be used in a terminal window:

    ```echo | openssl s_client -servername www.accessnow.org -connect www.accessnow.org:443 | openssl x509 -fingerprint -noout (replace the website URL)```

- The certificate fingerprint can be checked using a browser by following the instructions [here](https://www.a2hosting.com/kb/security/ssl/a2-hostings-ssl-certificate-fingerprints).

- Browser extensions can also be used to verify a certificate, and they can also notify the user if a certificate changes for any reason. [CheckMyHTTPS](https://chrome.google.com/webstore/detail/checkmyhttps/jbnodnfpdcegpnflleanllmiihkinkio?hl=en) can be used for Chrome-based browsers, and [Certificate Pinner](https://addons.mozilla.org/en-US/firefox/addon/certificate-pinner/) can be used for Firefox.



* * *

### Related Articles

[Article #165: Create or prolong/renew an SSL certificate](165-Create_or_prolong/renew_an_SSL_certificate)
