---
title: Tech Platforms Intervention in Legal Cases
keywords: law enforcement, contacts, litigation
last_updated: April 27, 2023
tags: [forensics, articles, faq]
summary: "A beneficiary requests intervention in pushing platforms to answer requests from law enforcement."
sidebar: mydoc_sidebar
permalink: 450-Tech_Platforms_Legal_Interventions.html
folder: mydoc
conf: Public
ref: Tech_Platforms_Legal_Interventions
lang: en
---


# Tech Platforms Intervention in Legal Cases
## How to deal with requests asking that we push platforms to answer requests from law enforcement

### Problem

Beneficiaries of the Helpline or their legal representatives, particularly those who work on Human Rights litigation or provide legal support to Human Rights Defenders, will sometimes reach out to ask for contacts of and connections to tech platforms (i.e. Meta, Google, TikTok, etc.) for the purposes of obtaining authenticated user account data  (i.e. IP Addresses, messages, etc) for litigation purposes (e.g. a copy of all the users' account data with the proper authentication from the platform that this is indeed that person's account and data).

Platforms have a global reach, but mechanisms to request legal evidence do not always exist, or require the existence of a [Mutual Legal Assistance Treaty](https://en.wikipedia.org/wiki/Mutual_legal_assistance_treaty) for information to be shared.

This article provides guidance on how to respond to requests for contacts or connections with platforms.

* * *


### Solution

#### For requests of someone else's account data

As far as we know, for all major platforms (especially those based in the US or the EU), there is no way for an individual or an organization to request another person's account data except through the platform's official law enforcement channel. These channels are reserved for law enforcement or other governmental legal authorities, and we have no visibility into this data or ability to request any level of access to such data. This is usually due to the data protection and privacy laws and the companies' own privacy and security policies that require them to take special measures to secure the privacy of users' account information. 

It is worth noting that at Access Now we are in favor of strong data protection laws and data minimization practices. While this can affect law enforcement cases where Helpline beneficiaries are the ones looking for the adversaries' account data, on the flipside, it also reduces the amount of data available to adversaries to, among other things, criminalize Human Rights activism. In this [resource](https://www.accessnow.org/issue/data-protection/), developed by the Access Now Advocacy arm, you will find more information on why data protection is important.

For cases that are being investigated or prosecuted outside of the United States, companies usually require the existence of a Mutual Legal Assistance Treaty between the US (where the company's headquarters are) and the country of the entity that is making the legal request. Depending on the jurisdiction and the existing treaties, there might be a better channel of collaboration between platforms and domestic legal authorities.

- [Meta](https://www.facebook.com/safety/groups/law/guidelines)
- [Google](https://lers.google.com/signup_v2)
- [TikTok](https://www.tiktok.com/legal/page/global/law-enforcement/en)
- [Apple](https://www.apple.com/privacy/government-information-requests/)

While we have no ability to facilitate the sharing of information through legal requests, it is usually possible to get some information about the level of collaboration between platforms  and specific governments through their transparency reports, which include aggregated data of government requests that platforms receive each year. For example:

- [Meta]( https://transparency.fb.com)
- [Google](https://transparencyreport.google.com/?hl=en)
- [TikTok](https://www.tiktok.com/transparency/en/reports/)
- [Apple](https://www.apple.com/legal/transparency/)

#### For requests to preserve someone else's account 

Sometimes beneficiaries or their attorneys may also want platforms to at least preserve someone else's account data in the fear that by the time they get an official law enforcement request, the person may delete their account (or specific evidence from the account).

Unfortunately, data preservation requests also need to be made by official government legal authorities through law enforcement channels listed above. 

Also please note that platforms usually store information for a certain amount of time even after it's deleted by the users. Therefore, even if a proper law enforcement request is sent, some data may not be preserved or recovered if it was deleted within the timeframe set by the platform.

#### For requests of a beneficiary's own account data

Sometimes individuals or their attorneys want to request their own account data for a legal purpose (e.g. see ticket #41764). Unfortunately, for all major platforms that we know of, it is similarly not possible to get the full account data without an official law enforcement request through a proper channel, as described above. 

Luckily, however, some major platforms now allow users to view and download their own data. Such processes usually allow a user to access most of their data (e.g. timeline posts they made on their own page or comments they made on other people's posts). However, some data may not be accessible through this process (e.g. deleted data, changes to bio or other description information, e.g. see ticket [#41764](https://helpline.accessnow.org/Ticket/Display.html?id=41764)).

Examples of account data download processes:

- [Facebook activity review](https://www.facebook.com/help/289066827791446)
- [Facebook data download](https://www.facebook.com/help/212802592074644)
- [Instagram activity review](https://help.instagram.com/647230196639835)
- [Instagram data download](https://help.instagram.com/contact/505535973176353)
- [Twitter activity review](https://help.twitter.com/en/managing-your-account/using-the-tweet-activity-dashboard#:~:text=To%20access%20your%20Tweet%20activity,iPad%2C%20or%20Twitter%20for%20Android) 
- [Twitter data download](https://help.twitter.com/en/managing-your-account/accessing-your-twitter-data)
- [Google activity review](https://myactivity.google.com/myactivity?pli=1) 
- [Google data download](https://support.google.com/accounts/answer/3024190?hl=en) 
- [TikTok data download](https://support.tiktok.com/en/account-and-privacy/personalized-ads-and-data/requesting-your-data) 

Please also note that beneficiaries or their attorneys may also want platforms to authenticate user account information in court (e.g. through testimony or sworn affidavit). While platforms typically do not provide such services outside of the standard law enforcement procedure, in some jurisdictions (e.g. California, US) the law allows for the account owner, or any person with knowledge of the contents of the account, to authenticate account content. See, for example, some relevant Help Center information from Facebook [here](https://www.facebook.com/help/132049190206790) and [here](https://www.facebook.com/help/133221086752707). 

#### For Other Type of Tech Platform Legal requests

For other types of requests involving legal cases of tech platforms, please escalate to the Service Platform Analyst. Access Now Legal team (legal-hl@accessnow.org) can also provide additional guidance if necessary. 

* * *


### Comments



* * *

### Related Articles

