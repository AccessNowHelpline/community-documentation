---
title: Instructions on Disabling Subject Line Encryption in Thunderbird
keywords: Helpline procedures, templates, PGP, OpenPGP, encryption, Thunderbird, email, subject line, case handling policy
last_updated: January 05, 2023
tags: [helpline_procedures_templates, templates]
summary: "Email instructing the beneficiary how to turn off subject line encryption in Thunderbird."
sidebar: mydoc_sidebar
permalink: 444-Disable_Thunderbird_Subject_Encryption.html
folder: mydoc
conf: Public
ref: Disable_Thunderbird_Subject_Encryption
lang: en
---


# Instructions on Disabling Subject Line Encryption in Thunderbird
## How to turn off subject line encryption in Thunderbird

### Body

Hi {{ beneficiary name }},

This is {{ incident handler name }} with the Access Now Digital Security Helpline. It looks like your Thunderbird client is currently set to automatically encrypt the subject lines of your emails. Please disable subject line encryption in Thunderbird, as this will make it easier for us to follow your case.

To disable subject line encryption in Thunderbird:

1. Go to the 'Account Settings' menu, which can be found either in the 'Tools' or 'Edit' drop-down menu.
2. On the left side of the settings window, under the email account you're using to contact us, click End-to-End encryption.
3. Scroll to the bottom section titled 'Advanced Settings', then uncheck the 'Encrypt the subject of OpenPGP messages' option.

 Please let me know if you need further guidance with these steps. Once you've turned off subject line encryption, let me know and we'll get back to working on your case.

Best,
{{ incident handler name }}

* * *


### Related Articles
