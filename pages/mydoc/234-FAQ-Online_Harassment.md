---
title: FAQ - Online Harassment Targeting a Civil Society Member
keywords: harassment, gendered online violence, doxing, hate speech, spyware, online harassment, account hijacking, impersonation
last_updated: January 10, 2023
tags: [harassment, articles, faq]
summary: "A civil society member has been subjected to a hate speech campaign or other forms of harassment and gendered online violence"
sidebar: mydoc_sidebar
permalink: 234-FAQ-Online_Harassment.html
folder: mydoc
conf: Public
lang: en
---


# FAQ - Online Harassment Targeting a Civil Society Member
## How to deal with a case of online harassment against the requester, a colleague, or their organization

### Problem

A case of online harassment has been reported.

- The harassment could lead to digital, physical or psychological damage to the targeted person.
- The suffered damage could stop the survivor from working, silence them, or compromise the activities of their entire organization.
- In some countries, some types of harassment could cause the targeted person to be arrested.


* * *


### Solution

#### How we should deal with the requester

When someone is being harassed, they are often in a very delicate emotional
state, so we should put them at ease, treat them seriously, and ensure they know
that we believe them and are on their side.

People who are targeted by harassment can often feel shame and guilt, so it's good to let them know that they're not alone and that what is happening is not their fault.

The best way to start handling these cases is requesting an online meeting and
let the beneficiary talk, asking our questions in the most delicate way possible.
Often a call with no video is preferable than video-chat, as victims of
harassment might not want to show their faces. Please note that sometimes a survivor may not be willing to talk at all, or even
to give details on the attack. Therefore, we should also ask them if they have someone trusted we can talk to and who can answer our questions and document the case for them.

If the beneficiary prefers not to talk and does not have someone trusted who could discuss their case with us, it can still be useful to send:

- recommendations with [immediate measures that the beneficiary can take](#immediate_measures)
- follow-up messages with suggestions of things they can do later on

- **Initial Reply template**: For the initial reply to beneficiaries who are targeted by gendered online violence, you can use the template in [Article #268: Initial Reply for Harassment Cases](268-Initial_Reply_Harassment.html).
- **Vetting template**: For introducing the vetting process to them, you can use the template in [Article #269: Inform Clients Targeted by Harassment about Vetting](269-Vetting_Info_Clients_Harassment.html).


#### Important questions we should ask

- The first question we should ask regards the identity of the attacker - often persons targeted by online harassment know or can imagine who the harasser is. Determining this is very important to identify the type of attack and also to define if the case is within our mandate.

- It is useful to ask the beneficiary if they can recall any incident that happened before and might be connected to the current attacks.

- To identify the best mitigation strategy, we should try to understand what kind of harassment we are dealing with, by answering the following questions:
    - Is the harasser a single person or a group?
    - Is the harassment targeting an individual or a group?
    - Has the harasser hacked into personal accounts?
    - Has the harasser published details on the beneficiary's personal life?
    - Has the harasser published private contents or images of the beneficiary?
    - Is the harasser impersonating the beneficiary?
    - Is the harassment based on attributes such as race, gender or religion?
    - Are the beneficiary and/or the harasser/s minors?
    - Has the harasser targeted the beneficiary's website?

Based on the answers to these questions, we can identify what kind of harassment is taking place and find the best way to address it (*see below, ["Typologies of
harassment and solutions"](#typology)*).

To identify what kind of harassment the beneficiary is suffering, we can use [this framework by Online SOS](https://onlinesos.org/action-center/category:identify).

One important detail we should ask about is if the targeted person has talked to their family and/or close friends. This can help us make sure that the person has
some kind of support network and that they don't feel completely alone in this situation.


#### Check that the case fits our mandate

After we have talked to the beneficiary and assessed the incident, we can check whether the case fits our mandate.

We should check that:

- The targeted person is not a minor (*see ["Cyber bullying" below](#cyberbullying)*).

- The targeted person is part of the civil society.

- The beneficiary has been targeted by online harassment due their activism and/or opinions.

- The harassment could damage the beneficiary's reputation, safety, or work, or could   disclose more personal information such as their identity, location, network   of contacts, etc.

- The harassment aims at silencing the beneficiary’s voice.

- The attack is not due to private motivations (e.g. it is not being launched by an ex partner or other family members for personal reasons that are unrelated to the targeted person's activism or work).

    If the attack is due to private motivations, but could harm the beneficiary's reputation, safety, or work, we should still support them. Otherwise we should refer them to a specialized organization - *see ["Organisations handling online harassment" below](#organisations)*).

**Important:** Even if the case does not fit our mandate, we should share resources with the beneficiary and possibly refer them to a helpdesk specializing in
online harassment (*See ["Organisations handling online harassment" below](#organisations)*).

If we refer the beneficiary to a specialized helpdesk, we should avoid making the beneficiary feel revictimized by having to tell us their story from scratch again. We can ask the beneficiary if they prefer us to send information on the incident to the helpdesk, or if they would like to go through the details again with the organization we have referred them to.
<a name="immediate_measures">
</a>
#### Immediate measures

It is always helpful to suggest the beneficiary to document the attacks or any other incident, and give them tips on how to take screenshots, save messages they receive, etc.

- We can offer them some guidance on how to document the incident based on [this how-to](https://cyber-women.com/en/online-violence-against-women/lets-start-a-documentation-journal/).

- If the attacks are too violent or the recipient feels overwhelmed, they can ask someone they trust to document the incidents for them for a while. Since this implies handing over access to their social media accounts, they should trust deeply the person who will manage this documentation. Once they can regain control of their account, they should change their passwords.

It is key to help the beneficiary make a risk analysis so they can make decisions on what to do next. If they don't know who the aggressor is, we should help them try to identify who they might be, what type of resources they might have, and what might be their purpose and possible next steps. This will help them feel less immobilized, as in general they aren't able to do a risk analysis on their own.

To prevent further damage, we should suggest the beneficiary to enable 2-factor authentication, possibly not SMS-based, on all their accounts.

If the beneficiary is part of a group, we should suggest them to inform the group about what is going on, as sometimes harassers target several members of a group, and also because the group should be warned not to follow fake social media accounts.
<a name="typology">
</a>
#### Typologies of harassment and solutions

Online harassment can take many forms, including:

- Abusive videos, comments, messages
- Revealing someone’s personal information (doxing)
- Stalking, through OSINT research and/or through the usage of commercial spyware
- Maliciously recording someone without their consent
- Deliberately posting content in order to humiliate someone
- Making hurtful and negative comments/videos about another person
- Unwanted sexualization
- Incitement to harass someone
- Hacking into someone's account
- Reporting an account to a platform for taking it down with fake motivations

Online harassment can be divided into several types. What follows is a list of the types the Helpline can directly address:

- **Account hijacking** - the appropriation of an individual's or a group's email or social media account.
- **Fake abuse reports** - organized efforts to have an account or page on social networking platforms suspended or deactivated by sending a number of reports to the platform for an alleged abuse on part of the owner of the account or page.
- **Doxing** - a form of harassment that consists in publishing an individual's personal details, for example their physical address or official identity.
- **Non-consensual sharing of private content** - a form of harassment consisting in publishing media and pictures, often nude, relating to someone's private life. Media often label this attack as "revenge porn" - a term which we should avoid, as it highlights the attackers' point of view.
- **Impersonation** - the impersonation of an individual's or group's identity, generally through the creation of fake profiles.
- **Hate speech** - this form of harassment consists in speech aimed at causing harm, demeaning or attacking a person or group on the basis of attributes such as race, religion, ethnic origin, sexual orientation, disability, or gender.
- **Censorship** - attempts at silencing a person or organization by making their website unreachable, for example through DDoS attacks or defacement.
- **Malware infection** - stalkers and other harassers can infect their target's devices with commercial spyware, also called stalkerware or spyware.

Types and tactics of online harassment are described more extensively [ here ](http://www.womensmediacenter.com/speech-project/online-abuse-101/).

In general, if the harasser is using a web service, the targeted person or our contact point can find information on how to report the attack in the relevant web pages:

- **Facebook**:
    - ["What should I do if I see something I don't like on Facebook?"](https://www.facebook.com/help/408955225828742)
    - [“How to Report Things”](https://www.facebook.com/help/reportlinks/)

- **Google+**: ["Report abuse on Google+"](https://support.google.com/plus/answer/6320425)

- **Gmail**: [“Report a Gmail user who has sent messages that violate the Gmail Program Policies and/or Terms of Use”](https://support.google.com/mail/contact/abuse)

- **Blogger**: [Harassment report form](https://support.google.com/blogger/answer/3091837)

- **Youtube**: [Harassment and cyberbullying policy](https://support.google.com/youtube/answer/2802268/?hl=en&amp;authuser=0)

- **Twitter**: [“Online abuse”](https://support.twitter.com/articles/15794)

- **Skype**: ["How do I report abuse by someone in Skype?"](https://support.skype.com/en/faq/FA10001/how-do-i-report-abuse-by-someone-in-skype)

- **Instagram**: [Reporting harassment or bullying on Instagram](https://help.instagram.com/547601325292351)

- **Yahoo**: [FAQ for blocking and/or reporting threatening or harassing emails](https://help.yahoo.com/kb/SLN3403.html)

There are however more specific measures that can be taken, depending on the type of attack:

##### Doxing and Non-Consensual Sharing of Private Content

See [Article #298: Doxing and Non-Consensual Publication of Pictures and Media](298-Doxing_Non-Consensual_Publication.html).


##### Impersonation
- **Twitter**: [Report an account for impersonation](https://support.twitter.com/forms/impersonation)

- **Google+**: [Impersonation guidelines on Google+](https://support.google.com/plus/answer/2370152?hl=en)

- **Blogger**: [Impersonation report form](https://support.google.com/blogger/answer/82112?hl=en)

- **Instagram**: [FAQ on impersonation](https://help.instagram.com/446663175382270)


##### Hate Speech

Besides reporting the posts or pages through the official channels listed above, we should suggest that the beneficiary report and block the user.

In some cases, though, particularly if the beneficiary knows who the attacker is, the
targeted person might choose not to block the attacker, so that they can keep
track of what they share or say about them. We could ask the beneficiary if they have
friends or supporters who could keep track of this for them, so they can stop
exposing themselves to the aggression.

To be alerted about hate speech against them, we might recommend the beneficiary to set up a [Google alert](https://www.google.com/alerts) to be informed if something is published with their names.
<a name="cyberbullying">
</a>
##### Cyber bullying (out of mandate)

Some cases of harassment are out of mandate. The main example is **cyber
bullying**, which affects **minors**. The Helpline cannot deal with minors and
we should refer these cases to recognized organizations that work with minors.

Most platforms have strict policies against bullying:

- [Facebook FAQ on bullying ](https://www.facebook.com/help/420576171311103/)
- [Facebook Bullying Prevention Hub ](https://www.facebook.com/safety/bullying/)
- [A list of resources for reporting cyber bullying and harassment ](https://cyberbullying.org/report)
- For cases in the US, [this web page ](https://www.stopbullying.gov/what-you-can-do/teens/index.html) can be a good resource we can refer the beneficiary to.


##### Censorship

- To prevent or mitigate DDoS attacks, we can refer the beneficiary to
	- [Deflect](137-Set_Up_Deflect.html)
	- [Project Galileo](72-Project_Galileo_Onboarding.html)
	- [Project Shield](https://projectshield.withgoogle.com/public/)

- If the beneficiary's website has been defaced, we can follow [Article #232: Website Defaced](232-Website_Defaced.html).


##### Malware / Stalkerware / Spouseware

If the beneficiary's device may be infected by malware, we can follow the relevant steps in [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html). If the beneficiary's device is a Windows computer, see [Article #133: How to clean a malware-infected Windows machine](133-Clean_Malware_Windows.html).

Also see the following resources:

- [How to Protect Yourself from Creepy, Phone Snooping Spyware ](https://motherboard.vice.com/en_us/article/xymngz/how-to-protect-yourself-from-creepy-phone-snooping-spyware)
- [A tool by Security Without Borders to find and remove the FlexiSPY stalkerware ](https://ops.securitywithoutborders.org/flexispy/)
- [How to Check if an Android Phone has a Stalkerware Installed?](https://www.randhome.io/blog/2021/01/16/how-to-check-if-an-android-phone-has-a-stalkerware-installed/)
- [CETA's Step-by-step How-to guides](https://www.ceta.tech.cornell.edu/resources) - a list of materials, tools, and resources that CETA volunteers have created to help IPV survivors, support workers, and technologists discover and address tech-related risks.
#### Content on websites

If the harasser is using a website/server, either to send malicious emails or to upload harassing content, the beneficiary may need to contact the administrators or the hosting provider in order to stop or mitigate the attack.

The Helpline team can help identify the contact point using WHOIS to identify the server/website owner.


#### Inform the police

In some cases the targeted person may need to inform the police. The Helpline should assist the beneficiary to determine whether this would help or further damage their situation. Especially if the adversary is a part of or is linked to the government, contacting the authorities could only make things worse.

If the website/server used by the harasser does not accept to collaborate, the targeted person may need to contact the authorities of the host country to stop the harassment.
<a name="organisations">
</a>
#### Organisations handling online harassment

If the case is out of mandate, for example because the attack is connected to private motivations and the beneficiary is not a member of civil society, we can refer them to an organization that focuses on protecting users from online harassment:

- In **Pakistan**, the Digital Rights Foundation manages a Cyber Harassment Helpline
   - According to [their website](https://digitalrightsfoundation.pk/contact/), the Helpline can be reached at 0800-39393 or helpdesk@digitalrightsfoundation.pk
   - [Helpline Policy](http://digitalrightsfoundation.pk/wp-content/uploads/2017/02/Public-Policy-for-Helpline_30.11.2016-1.pdf)

- **Nicaragua**: [Enredadas](https://www.facebook.com/EnRedadasNi/)

- **Mexico**: [Dominemos las TIC](https://twitter.com/DominemosTicMx)
- **Ecuador**: [Navegando Libres Por La Red](https://www.navegandolibres.org/)
- **Latinamerica and USA**
    - [**Vita-Activa**](https://vita-activa.org/) - online support and strategic solutions for women and LGBTIQ+ journalists, activists and gender, land and labor rights, and freedom of expression defenders experiencing stress, trauma, crisis, burnout and/or facing gender based violence.

- **USA**:
    - [**Cyber Civil Rights Initiative**](https://www.cybercivilrights.org/contact-us/) - hotline for survivors of online abuse - US-based, according to their website can be reached by phone at: 1-844-878-2274

    - [Information & Resources for Survivors of Violence Against Women](https://nnedv.org/get-help/) (mainly in the U.S.)

    - [**The Games and Online Harassment Hotline**](http://gameshotline.org/) - online support in and around the gaming community. Text SUPPORT to 23368 (USA only) to get started with an agent during their open hours. Their website [gameshotline.org]( http://gameshotline.org/ ) has more resources for gaming specific aid and online security support.


- **Spain/Catalonia**: [Donestech](https://donestech.net) in 2019 produced [KIT Against online gender-based violence](https://donestech.net/files/kit_against_online_gender-based_violence_2019.pdf) which lists a number of Spanish and English resources. offers support to persons targeted by online violence.
- **Worldwide**:
    - [ **Take Back the Tech** ](https://www.takebackthetech.net/) is a global project to take control of technology to end violence against women, and offers support against online gendered violence. Accordingly to their [website](https://www.takebackthetech.net/contact), they receive help requests through the help@takebackthetech.net mailbox and do not offer phone support. They only accept referrals from known contacts.
- [A list of organizations with resources for individuals experiencing online harassment](https://iheartmob.org/resources/supportive_organizations)

    - More organizations are listed [here](https://gendersec.tacticaltech.org/wiki/index.php/Online_Harassment_Support_Initiatives).


* * *


### Comments

#### Additional resources

- [A list of manuals on privacy and security with a gender perspective](https://gendersec.tacticaltech.org/wiki/index.php/Manuals_with_a_gender_perspective)
- For preventing doxing, we can suggest the beneficiary to follow the instructions in Access Now Helpline's [Self-doxing guide](https://guides.accessnow.org/self-doxing.html).


##### English

- [The Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/) - a good resource for journalists on how to prevent and respond to harassment
- [ Info on blackmail-sextorsion, cyberstalking and hate speech ](https://www.takebackthetech.net/know-more)
- [ DIY Cybersecurity for Domestic Violence ](https://hackblossom.org/domestic-violence/)
- [ Heartmob ](https://iheartmob.org/) is a support network to fight harassment on social media - For starting to get support from the network, refer the beneficiary to [ this link ](https://iheartmob.org/target/sign_up).
- [ Heartmob webinar links and resources ](https://docs.google.com/document/d/158BfZIJFCz9tJwFIGkOYfWdPF-jjV2OEJNbF4j6sIgk/edit)
- [ Staying safe on social media ](https://iheartmob.org/resources/safety_guides)
- [ Online harassment resources ](https://www.ihollaback.org/resources/)
- [ Crash Override Network resources ](http://www.crashoverridenetwork.com/resources.html)
- [ Crash Override's Automated Cybersecurity Helper ](http://www.crashoverridenetwork.com/coach.html)
- [ More resources on online violence ](http://femtechnet.org/csov/)
- [PrivacyBot](https://privacybot.io/) - a free and open source downloadable tool that anyone can use to quickly delete their information from data broker sites

##### Spanish

- Acoso Online - [Pornografía No Consentida - Cinco claves para denunciar y resistir su publicación](https://acoso.online)

    Acoso Online aims to provide reliable information and tools to victims of non-consensual pornography as well as the organizations that accompany and support them. Their online resource covers different responses available to victims and their supporters - digital security, platform reporting, legal mechanisms, and social or community-oriented responses. They currently have information on their site for legal mechanisms in Chile, Argentina, Brazil, Peru, Venezuela, Panama and Mexico.

- [ Violencia contra las mujeres y tecnología: Estrategias de respuesta ](https://socialtic.org/blog/violencia-contra-las-mujeres-y-tecnologia-estrategias-de-respuesta/) - [PDF](https://socialtic.org/wp-content/uploads/2017/12/GuiaEstrategias_Ciberseguras.pdf)


* * *

### Related Articles

- [Article #268: Initial Reply for Harassment Cases](268-Initial_Reply_Harassment.html)
- [Article #269: Inform Clients Targeted by Harassment about Vetting ](269-Vetting_Info_Clients_Harassment.html)
- [Article #298: Doxing and Non-Consensual Publication of Pictures and Media](298-Doxing_Non-Consensual_Publication.html)
- [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html)
- [Article #133: How to clean a malware-infected Windows machine](133-Clean_Malware_Windows.html)
- [Article #72: Project Galileo onboarding of new client - DDoSP](72-Project_Galileo_Onboarding.html)
- [Article #137: How to set up Deflect DDoSP for a client](137-Set_Up_Deflect.html)
- [Article #232: Website Defaced](232-Website_Defaced.html)
