---
title: How to Circumvent the Great Firewall in China
keywords: circumvention, Great Firewall, China, VPN, Tor, censorship
last_updated: November 1, 2021
tags: [anonymity_circumvention, articles]
summary: "Client needs to overcome the Great Firewall in Mainland China"
sidebar: mydoc_sidebar
permalink: 110-VPN_China.html
folder: mydoc
conf: Public
lang: en
---


# How to Circumvent the Great Firewall in China
## VPN recommendations for Mainland China

### Problem

A client needs to circumvent the Great Firewall (GFW), either because they live, or are travelling to, Mainland China, but:

- VPN servers are frequently blocked in Mainland China.
- There is evidence that the Great Firewall has the ability to actively probe and block VPN traffic.
- Some VPN providers do not respect users' privacy.
- Tor is blocked in Mainland China.

* * *


### Solution

#### VPNs

The solution we propose to the client will depend on what they need.

If the user is just interested in circumvention, they can use a VPN.

Sometimes some of these tools will work in some regions or circumstances and not in others, and some of them may be blocked at the ISP level. The best strategy is to set up several VPNs before the client travels to China so that they have more than one option in case one doesn't work.

A good resource to identify available and functional VPN services in China is [GreatFire](https://cc.greatfire.org/en).

Another resource is [GFW Report](https://gfw.report/) which analyses how the GFW identifies Shadowsocks-based VPN servers and provides guidance to avoid detection for [servers](https://gfw.report/blog/ss_tutorial/en/) and [users](https://gfw.report/blog/ss_advise/en/).

#### Tor

The client should install Tor Browser before they travel, since the Tor Project website is blocked in China. They can also use the [GetTor](https://gettor.torproject.org/) service, which will email them download links.

In China direct connections to the Tor network are blocked. To circumvent the Great Firewall, it is necessary to use a Tor bridge. A how-to for using Tor bridges in China can be found
[here](https://support.torproject.org/censorship/connecting-from-china/).

Sometimes, the built-in bridges in Tor Browser and the email service through bridges@torproject.org don't work for users in China. However, private bridges will usually still work. Instructions on how to use a private bridge can be found [here](https://tb-manual.torproject.org/bridges/).
If a client needs further help getting a private bridge address, they can contact the Tor Project by emailing their support team at frontdesk@torproject.org.

  - [FAQ on circumventing censorship using Tor Browser](https://support.torproject.org/censorship/)
  - [Information on circumvention and pluggable transports](https://tb-manual.torproject.org/circumvention/)
  - [How to add bridges in Tor Browser](https://tb-manual.torproject.org/bridges/)
  - Article on what to do when [Tor is blocked in a country](https://blog.torproject.org/blog/breaking-through-censorship-barriers-even-when-tor-blocked) (published August 3rd, 2016)
  - [Another Tor Project guide on how to setup bridges](https://bridges.torproject.org/)
  - [A community-maintained guide to using bridges and setting up a bridge relay](https://sigvids.gitlab.io/)
#### Other apps with pluggable transports



* * *


### Comments


* * *

### Related Articles

- [Article #175: FAQ - Circumvention &amp; Anonymity tools](175-Circumvention_Anonymity_tools_list.html)
