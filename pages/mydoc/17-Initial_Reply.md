---
title: Initial Reply in English
keywords: email templates, initial reply, case handling policy
last_updated: February 3, 2023
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client"
sidebar: mydoc_sidebar
permalink: 17-Initial_Reply.html
folder: mydoc
conf: Public
ref: Initial_Reply
lang: en
---


# Initial Reply in English
## First response, Email to English-speaking Client


### Body


Dear {{ beneficiary name }},

Thank you for contacting the Access Now Digital Security Helpline (https://www.accessnow.org/help). My name is {{ incident handler name }} and I am here to help you.

Information about Safer Communications

We understand that our communications may be sensitive, so we encourage you to establish an end-to-end encrypted channel of communications. If you already use PGP for encrypted email, please send us your public key. You can find ours at https://keys.accessnow.org/help.asc. Otherwise please let us know if you use any other end-to-end encrypted communications channel, such as Signal messenger. You can follow this guide to install Signal: https://support.signal.org/hc/en-us/articles/360008216551-Installing-Signal

If you aren't familiar with these tools, we’ll be happy to guide you through setting one up so that we can communicate safely.

Information about Vetting

It’s important to understand that we vet new potential Helpline beneficiaries. This means we reach out to our trusted contacts and share your name, e-mail address, and organization with them – we do this to confirm that you are who you say you are, that the contact information provided is in fact associated with you, and that you are part of civil society, which includes activists, journalists & bloggers, and human rights defenders.

We do not under any circumstances share why you’re contacting us or any information related to your request for support, which will be kept strictly confidential within the Helpline team.

If you have any questions or concerns about the vetting process, or if you need to opt out of this process, you can respond to this email and let us know. Opting out of the vetting process may deprioritize our support for you and reduce the ways we can assist you, but we will still try to help where we can.

Action Requested - Complete our Vetting Survey

To help us support you quickly, please fill out the following survey, where you’ll be asked to provide two references who can confirm your contact details and civil society work, as well as some basic information about you and the scope of your work:

https://form.accessnow.org/index.php/1?lang=en/Y?ticket={{ vetting ticket id }}

This form is confidential between us, is hosted on Helpline infrastructure, and is not accessible by other parties.

Take care,
{{ incident handler name }}

* * *

### Comments

* * *

### Related Articles

- [Article #225: FAQ - Initial Reply in Foreign Languages](225-FAQ-Initial_Replies_Multilanguage.html)
- [Article #448: Initial Reply in English for a Vetted Beneficiary](448-Initial_Reply_English_Vetted.html)
