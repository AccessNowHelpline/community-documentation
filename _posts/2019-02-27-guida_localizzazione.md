---
title:  "Turning the Documentation Theme for Jekyll into a Multilanguage Theme"
categories: multilanguage
permalink: it_localization_guide.html
tags: [news]
---

## IDEA:

* retrocompatibilità con i yml che già ci sono
* retrocompatibilità con le pagine che già ci sono
* flessibilità dei meccanismi di traduzione
* parsimonia di meccanismi
* en come lingua di fallback

Ispirazione: [Come tradurre un sito in jeckyll senza plugin](https://www.sylvaindurand.org/making-jekyll-multilingual/)

TEMA DI PARTENZA: [Tema jekyll per documentazione di Idra Therbe](https://idratherbewriting.com/documentation-theme-jekyll/)

Realizzazione: [Documentazione comunitaria della Helpline di Access Now](https://accessnowhelpline.gitlab.io/community-documentation/)

## PARTI TRADOTTE:

pagine: pagine, home page (Liquid sintax)

menu: sidebar e top/navbar (Liquid sintax)

tag: descrizione e link (Liquid sintax)

componenti: summary, search, titolo sitoweb, .. (Liquid sintax, CSS)

feature:
* supporto per lingue RTL (JS)
* script prima volta in home (JS)
* script dei pulsanti cambio lingua (JS)
* script link della home page nel titolo del sitoweb (Liquid sintax)
* le liste dei TAG fanno selezione in base alla lingua (Liquid sintax)
* nel menù laterale vengono generati i link delle url in base alla lingua (Liquid sintax)

## RIASSUNTO:

pagine:
funzionano con il sistema ref e lang, ovvero un identificativo (ref) uguale tra pagine e un lang per indicare la lingua (2 cifre).
La home page segue lo stesso sistema, le pagine della home page tradotta si trovano nella partella pages come tutte le altre.

menu:
Il multilanguage dei titoli funziona mettendo nel campo del titolo “multilanguage” e a seguire allo stesso livello di indentazione, chiave:valore; con chiave l’id della lingua.
Per i link interni dentro a sidebar si mette l’identificativo della lingua e vengono creati i link id_titolo.html
Per i link esterni dentro a topnav si aggiunge multilanguage e poi si crea il figlio m_externalurl e quindi i figli sono gli identificativi delle lingue.

tag:
I tag similmente per le pagine, vanno creati dei file nuovi, con ref, lang e langNow.
La url deve essere da modello it_nometag.md

componenti: 
si trovano in _data/config.yml e string.yml ed alcuni nel CSS nel file css/customstyle.css

[Documentazione specifica sito Access Now](./community-documentation/come_tradurre_questo_sitoweb.html)

## Script nel dettaglio:

### script dei pulsanti cambio lingua 

(JS) nel file [community-documentation/_layouts/default.html](https://gitlab.com/krst/community-documentation/blob/master/_layouts/default.html#L7) 
{% raw %}
    
        var languages = ["ar", "de", "en", "es", "fl", "fr", "it", "pt", "ru"];
    
        var languageshomeredirect = ["ru","de","it"];
        //sostit with Liquid sintax?
    
        var written = new Array(9 + 1).join('0').split('');
    
        var langind = 0;
    
        var towrite = [];
    
        var i = 0;

        function listaWritten() {
            $("ul.multilang li").each(function(index) {
                var langex = $(this).text();
                written[index] = langex.trim();
            });
            //console.log(written);
        }

        function writeAllLang() {
            $("ul.multilang li").remove();
            for (a = 0; a < languages.length; a = a + 1) {
                var singlelang = towrite[a];
                $("ul.multilang").append(singlelang);
            };
        }

        function addMultilangLink() {
            listaWritten();
            for (i = 0; i < languages.length; i = i + 1) {

                if (languages[i].trim() == written[langind]) {
                    var htmlwritten = $("ul.multilang li");
                    //console.log("nothing to do" + htmlwritten[langind] + langind);
                    var writenow = $(htmlwritten[langind]).html();
                    //console.log("to do" + writenow);
                    towrite.push("<li>" + writenow + "</li>");

                    langind = langind + 1;
                } else {
                    //$("ul.multilang").append('<a href="./how_translate_this_website.html" class="tradu">' + languages[i] + '</a>');
                    towrite.push('<li><a href="#" class="tradu">' + languages[i] + '</a></li>');
                }
            };

            writeAllLang();
            //onclick appear #wannat
            $('.tradu').click(function() {
                $('#wannat').removeClass("hide")
            });

        }; /* cicla i li di ul.multilang e cerca se hanno la classe con la lingua; se esiste.. bene, se non esiste, append un li standard.*/
{% endraw %}

### supporto per lingue RTL 

(JS) nel file [community-documentation/_layouts/default.html](https://gitlab.com/krst/community-documentation/blob/master/_layouts/default.html#L58)
{% raw %}
    
         var rtllang= [
    "ar",  // Arabic
    "dv",  // Divehi; Dhivehi; Maldivian
    "he",  // Hebrew (modern)
    "fa",  // Persian
    "ps",  // Pashto; Pushto
    "ur",  // Urdu
    "yi",  // Yiddish
    ];
        function toggleMultilangRtl(){
            var languageset= $("html").attr("lang");
           //languageset= languageset.trim();
            for(i=0;i<rtllang.length;i++){
            if (languageset == rtllang[i]){
                $(".post-content").addClass("multilanguagertl");
                $(".post-header").addClass("multilanguagertl");
                return true;                
                }
        else{
              $(".post-content").removeClass("multilanguagertl");
            $(".post-header").removeClass("multilanguagertl");
            }
            };
        };
        
        $(function() {
           
            // multilang init
            addMultilangLink();
            // multilang rtl-ltr
            toggleMultilangRtl();
        });

{% endraw %}

### script prima volta in home 

(JS) nel file [community-documentation/_layouts/default.html](https://gitlab.com/krst/community-documentation/blob/master/_layouts/default.html#L145)

{% raw %}
      
      function getBrowserLang(){
           var writelocallang=localStorage.getItem("languageid");
               
           if(writelocallang==null){
           var userlang = navigator.language || navigator.userLanguage; 
           
           //se non c'è niente in localstorage, fai redirect, altrimenti..niente.
           userlang=userlang.slice(0, 2);
               
           localStorage.setItem("languageid", "");
               
               for(i=0;i<languageshomeredirect.length;i++){
                   //console.log(languageshomeredirect);
                   if (userlang == languageshomeredirect[i]){
                   window.location.replace(userlang+"_index.html");
               };
               }           
           }
        //console.log("niente");
       }
        $(function() {
            getBrowserLang();   
        });
 
    
{% endraw %}
### script link della home page nel titolo del sitoweb

(Liquid sintax) nel file [community-documentation/_includes/topnav.html](https://gitlab.com/krst/community-documentation/blob/master/_includes/topnav.html#L11)
{% raw %}

    {% if site.topnav_title == "multilanguage"%}
        {% if site[page.lang] and [page.lang != "en"]%}
       <a class="fa fa-home fa-lg navbar-brand" href="{{[page.lang]_index.html}}">&nbsp;<span class="projectTitle">A{{site[page.lang]}}</span></a>
       {% else %}
       <a class="fa fa-home fa-lg navbar-brand" href="index.html">&nbsp;<span class="projectTitle">{{site.en}}</span></a>
       {% endif %}
       {% else %}
       <a class="fa fa-home fa-lg navbar-brand" href="index.html">&nbsp;<span class="projectTitle"> {{site.topnav_title}}</span></a>
    {% endif %}      

{% endraw %}

### logica dei TAG 

fa selezione in base alla lingua (Liquid sintax) nel file [community-documentation/_includes/taglogic.html](https://gitlab.com/krst/community-documentation/blob/master/_includes/taglogic.html#L13)

{% raw %}
    
       {% assign thisTag = page.tagName %}
       {% assign thisLang = page.langNow %}        
    {% for page in site.pages %}
    {% for tag in page.tags %}
      {% for lang in page.lang %}
        {% if tag == thisTag and lang == thisLang %} 

        <tr><td><a href="{{ page.url | remove: "/" }}">{{page.title}}</a></td>
            <td><span class="label label-default">Page</span></td>
          <td>{% if page.summary %} {{ page.summary | strip_html | strip_newlines | truncate: 160 }} {% else %} {{ page.content | truncatewords: 50 | strip_html }} {% endif %}</td>
        </tr>
        {% endif %}
       {% endfor %}
      {% endfor %}
      {% endfor %}
 
{% endraw %}   

### menù laterale 

vengono generati i link delle url in base alla lingua (Liquid sintax) nel file [community-documentation/_includes/sidebar.html](https://gitlab.com/krst/community-documentation/blob/master/_includes/sidebar.html#L2)

{% raw %}
    
    {% assign thisLang = page.lang %}

    <ul id="mysidebar" class="nav">
        {% if sidebar[0].product == "multilanguage" %}
                {% if sidebar[0][page.lang] %}  
    <li class="sidebarTitle">{{sidebar[0][page.lang]}} {{sidebar[0].version}}</li>
                {% else %}
    <li class="sidebarTitle">{{sidebar[0].en}} {{sidebar[0].version}}</li>
                {% endif %}
        {% else %}
    <li class="sidebarTitle">{{sidebar[0].product}} {{sidebar[0].version}}</li>
        {% endif %}
    {% for entry in sidebar %}
    {% for folder in entry.folders %}
    {% if folder.output contains "web" %}
    <li>
        {% if folder.title == "multilanguage" %}
            {% if folder[page.lang] %}  
      <a href="#">{{ folder[page.lang] }}</a>
            {% else %}
      <a href="#">{{ folder.en }}</a>
            {% endif %}
        {% else %}
      <a href="#">{{folder.title}}</a>
        {% endif %}
      <ul>
          {% for folderitem in folder.folderitems %}
          {% if folderitem.output contains "web" %}
          {% if folderitem.external_url %}
          <li><a href="{{folderitem.external_url}}" target="_blank">{{folderitem.title}}</a></li>
          {% elsif page.url == folderitem.url %}
          <li class="active"><a href="{{folderitem.url | remove: "/"}}">{{folderitem.title}}</a></li>
          {% elsif folderitem.type == "empty" %}
          <li><a href="{{folderitem.url | remove: "/"}}">{{folderitem.title}}</a></li>

         {% elsif thisLang == "en" %} <!--english-->
                    <li><a href="{{folderitem.url | remove: "/"}}">{{folderitem.title}}</a></li>

          {% elsif folderitem.lang contains thisLang %} <!--multilang-->
                    <li><a href="{{thisLang}}_{{ folderitem.url | remove: "/"}}">{{folderitem.title}}</a></li>
          {% endif %}
          {% for subfolders in folderitem.subfolders %}
          {% if subfolders.output contains "web" %}
          <li class="subfolders">
            {% if subfolders.title == "multilanguage" %}
                {% if subfolders[page.lang] %}  
              <a href="#">{{ subfolders[page.lang] }}</a>
                {% else %}
              <a href="#">{{ subfolders.en }}</a>
                {% endif %}
            {% else %}
              <a href="#">{{subfolders.title}}</a>
        {% endif %}
              <ul>
                  {% for subfolderitem in subfolders.subfolderitems %}
                  {% if subfolderitem.output contains "web" %}
                  {% if subfolderitem.external_url %}
                  <li><a href="{{subfolderitem.external_url}}" target="_blank">{{subfolderitem.title}}</a></li>
                  {% elsif page.url == subfolderitem.url %}
                  <li class="active"><a href="{{subfolderitem.url | remove: "/"}}">{{subfolderitem.title}}</a></li>
                  {% elsif thisLang == "en" %} <!--english-->
                                    <li><a href="{{subfolderitem.url | remove: "/"}}">{{subfolderitem.title}}</a></li>
                  {% elsif folderitem.lang contains thisLang %} <!--multilang-->
                                    <li><a href="{{subfolderitem.url | remove: "/"}}">{{subfolderitem.title}}</a></li>
                  {% endif %}
                  {% endif %}
                  {% endfor %}
              </ul>
          </li>
          {% endif %}
          {% endfor %}
          {% endif %}
          {% endfor %}
      </ul>
      </li>
        {% endif %}
      {% endfor %}
      {% endfor %}

{% endraw %}  

### menù in alto /navbar

è possibile tradurre i titoli ed anche avere link diversi per ogni lingua, (Liquid sintax) nel file [community-documentation/_layouts/topnav.html](https://gitlab.com/krst/community-documentation/blob/master/_includes/topnav.html#L26)

{% raw %}
    
    {% assign topnav = site.data[page.topnav] %}
    {% assign topnav_dropdowns = site.data[page.topnav].topnav_dropdowns %}
                {% for entry in topnav.topnav %}
                {% for item in entry.items %}
                {% if item.title == "multilanguage"%}
                        {% if item[page.lang] %}
                        <li><a href="{{item.url | remove: "/"}}">{{item[page.lang]}}</a></li>
                        {% else %}
                        <li><a href="{{item.url | remove: "/"}}">{{item.en}}</a></li>
                        {% endif %}
                {% elsif item.external_url %}
                <li><a href="{{item.external_url}}" target="_blank">{{item.title}}</a></li>
                {% elsif page.url contains item.url %}
                <li class="active"><a href="{{item.url | remove: "/"}}">{{item.title}}</a></li>
                {% else %}
                <li><a href="{{item.url | remove: "/"}}">{{item.title}}</a></li>
                {% endif %}
                {% endfor %}
                {% endfor %}
                <!-- entries with drop-downs appear here -->
                <!-- conditional logic to control which topnav appears for the audience defined in the configuration file.-->
                {% for entry in topnav_dropdowns %}
                {% for folder in entry.folders %}
                <li class="dropdown">
                {% if folder.title == "multilanguage"%}
                        {% if folder[page.lang] %}
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ folder[page.lang] }}<b class="caret"></b></a>
                        {% else %}
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ folder.en }}<b class="caret"></b></a>
                        {% endif %}
                {% else %}
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ folder.title }}<b class="caret"></b></a>
                {% endif %}
                    <ul class="dropdown-menu">
                        {% for folderitem in folder.folderitems %}
                        {% if folderitem.title == "multilanguage"%}
                            {% if folderitem[page.lang] %}
                                {% if folderitem.external_url=="multilanguage"%}
                                    {% if folderitem.m_external_url[page.lang]%}
                                            <li><a href="{{folderitem.m_external_url[page.lang]}}" target="_blank">{{folderitem[page.lang]}}</a></li>
                                    {% else %}
                                            <li><a href="{{folderitem.m_external_url.en}}" target="_blank">{{folderitem.en}}</a></li>
                                    {% endif%}
                                 {% elsif page.url contains folderitem.url %}
                                <li class="dropdownActive"><a href="{{folderitem.url |  remove: "/"}}">{{folderitem[page.lang]}}</a></li>
                                {% elsif folderitem.url %}
                                <li><a href="{{folderitem.url | remove: "/"}}">{{folderitem[page.lang]}}</a></li>
                                {% elsif folderitem.external_url!="multilanguage" %}
                                <li><a href="{{folderitem.external_url}}" target="_blank">{{folderitem[page.lang]}}</a></li>                        {% endif %}
                        {% elsif folderitem.external_url=="multilanguage"%}
                                    {% if folderitem.m_external_url[page.lang]%}
                                            <li><a href="{{folderitem.m_external_url[page.lang]}}" target="_blank">{{folderitem.en}}</a></li>
                                    {% else %}
                                            <li><a href="{{folderitem.m_external_url.en}}" target="_blank">{{folderitem.en}}</a></li>
                                    {% endif%}
                        {% elsif folderitem.external_url%}
                        <li><a href="{{folderitem.external_url}}" target="_blank">{{folderitem.en}}</a></li>
                        {% elsif page.url contains folderitem.url %}
                        <li class="dropdownActive"><a href="{{folderitem.url |  remove: "/"}}">{{folderitem.en}}</a></li>
                        {% else %}
                        <li><a href="{{folderitem.url | remove: "/"}}">{{folderitem.en}}</a></li>
                        {% endif %}
                        {% elsif folderitem.external_url %}
                        <li><a href="{{folderitem.external_url}}" target="_blank">{{folderitem.title}}</a></li>
                        {% elsif page.url contains folderitem.url %}
                        <li class="dropdownActive"><a href="{{folderitem.url |  remove: "/"}}">{{folderitem.title}}</a></li>
                        {% else %}
                        <li><a href="{{folderitem.url | remove: "/"}}">{{folderitem.title}}</a></li>
                        {% endif %}
                        {% endfor %}
                    </ul>
                </li>
                {% endfor %}
                {% endfor %}
{% endraw %}



::::::::::::::::::::::::::::::::::::::

## Futuri possibili TO DO:

* adeguamenti per i post oltre che le page

* in traduzioni sul sito manca da spiegare i menu

* bisogna aggiungere lasciare vuoto e mettere sotto chiave:valore;  il  default. Ora en è harcodato.

[https://github.com/sylvaindurand/jekyll-multilingual/issues/4](https://github.com/sylvaindurand/jekyll-multilingual/issues/4)

* generale miglioramento del codice :D

* automatica rilevazione di una nuova lingua

* togliere i cdn per jquery?





{% include links.html %}
