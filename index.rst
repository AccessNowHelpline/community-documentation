.. this is the index file that tells Sphinx/RtD how to build the static site at https://maggie-documentation.readthedocs.io/en/latest/ .


Access Now Helpline: Community Documentation
==============================================

Welcome to Access Now's Community Documentation.
You can contribute to this documentation through Gitlab [here](https://gitlab.com/AccessNowHelpline/community-documentation).

Account Security
=================

.. toctree::
   :maxdepth: 2

   Hotmail Account Blocked for Security Reasons <pages/mydoc/>
   Secure Yahoo Account with 2-Step Verification <pages/mydoc/>
   Recommendations on Two-Factor Authentication <pages/mydoc/>
   Recommendations on Team Password Managers <pages/mydoc/>
   Securing Facebook Pages Admin Accounts <pages/mydoc/>
   Google Account Security for an Individual <pages/mydoc/>
   FAQ - Securing Online Accounts <pages/mydoc/>

Anonymity and Circumvention
===========================

.. toctree::
   :maxdepth: 2

   How to Circumvent the Great Firewall in China <pages/mydoc/>
   Force Tor Browser to use specific exit nodes <pages/mydoc/>
   Circumvention & Anonymity Tools List <pages/mydoc/>

Browsing Security
=================

.. toctree::
   :maxdepth: 2

   Safe Browsing Practices and Plugins <pages/mydoc/>

Censorship
==========

.. toctree::
   :maxdepth: 2

   Website Censorship in China <pages/mydoc/>
   Android App Removal <pages/mydoc/>

Data Leaks
==========

.. toctree::
   :maxdepth: 2

   FAQ - Data Leak <pages/mydoc/>

DDoS Attack
===========

.. toctree::
   :maxdepth: 2

   How to set up Deflect DDoSP for a client <pages/mydoc/>
   DDoS Attack Identification <pages/mydoc/>

Defacement
==========

.. toctree::
   :maxdepth: 2

   Website Defaced <pages/mydoc/>

Devices and Data Security
=========================

.. toctree::
   :maxdepth: 2

   Full-Disk Encryption on Mac with FileVault 2 <pages/mydoc/>
   Tails Boot Issues on Mac <pages/mydoc/>
   Antivirus for Mac <pages/mydoc/>
   Secure File Sharing on Google Drive <pages/mydoc/>
   Security Measures for macOS Computers <pages/mydoc/>
   FAQ - Full-Disk Encryption (FDE) <pages/mydoc/>
   FAQ - Secure Backup <pages/mydoc/>
   Encrypt an External Storage Device Using Disk Utility on Mac <pages/mydoc/>
   File Encryption with VeraCrypt or GPG <pages/mydoc/>
   Encrypt an External Storage Device with VeraCrypt <pages/mydoc/>
   Recommendations on Secure File Sharing and File Storage <pages/mydoc/>
   How to Check the Integrity of a File <pages/mydoc/>
   Encrypt Files on a Mac with GpgTools <pages/mydoc/>
   Ubuntu - Linux - FDE after OS installation <pages/mydoc/>
   FDE with DiskCryptor on Windows <pages/mydoc/>

Templates

.. toctree::
   :maxdepth: 2

   Instructions to set Up FileVault 2 on a Mac <pages/mydoc/103-FileVault2_Mac.md>

Direct Intervention
===================

.. toctree::
   :maxdepth: 2

   Training Resources <pages/mydoc/>

Documentation
=============

.. toctree::
   :maxdepth: 2

   Converting Markdown to InDesign and Vice Versa <pages/mydoc/>

Fake Domain Mitigation
======================

.. toctree::
   :maxdepth: 2

   Handling of Fake Domains <pages/mydoc/>

Forensics
=========

.. toctree::
   :maxdepth: 2

   Online Tools to Check a Websites' Reputation <pages/mydoc/140-websites_check_IP_reputation.md>
   Forensic Handling of Data on a PC <pages/mydoc/>
   Android Devices Data Acquisition Procedure <pages/mydoc/>
   Mobile Data Acquisition Report Guidelines <pages/mydoc/>
   Data Acquisition Using Android Debug Bridge (ADB) <pages/mydoc/>
   Forensic Analysis of Videos and Images <pages/mydoc/>
   Host-Based Live Forensics on Windows <pages/mydoc/>
   Host-Based Live Forensics on Linux/Unix <pages/mydoc/>
   MS Office Files static analysis <pages/mydoc/>
   PCAP File Analysis with Wireshark to investigate Malware infection <pages/mydoc/>

Harassment
==========

.. toctree::
   :maxdepth: 2

   FAQ - Online Harassment Targeting a Civil Society Member <pages/mydoc/234-FAQ-Online_Harassment.md>
   Doxing and Non-Consensual Publication of Pictures and Media <pages/mydoc/298-Doxing_Non-Consensual_Publication.md>

Helpline Procedures
===================

.. toctree::
   :maxdepth: 2

   FAQ - Initial Reply in Foreign Languages <pages/mydoc>
   Outreach to Clients for Local Situations <pages/mydoc>
   Protocol for a Client We Suspect Is Paranoid <pages/mydoc>

Infrastructure
==============

.. toctree::
   :maxdepth: 2

   Report URL miscategorization <pages/mydoc/149-Report_URL_miscategorization.md>
   Reboot a Website under OVH Using FileZilla <pages/mydoc/151-Reboot_Website_OVH_FileZilla.md>
   Recommend CMS/Framework for NGOs <pages/mydoc/184-Recommend_CMS-Framework_for_NGOs.md>
   Recommendations for Domain Registration and Disputes <pages/mydoc/353-Recommendations_Domain_Registration.md>
   Project Management Tools for NGOs <pages/mydoc/365-Project_Management_Tools.md>
   Google Workspace (formerly G Suite) Domain Security Review & Hardening <pages/mydoc/381-Google-Workspace-Domain-Security.md>
   TLS and self-signed certificates <pages/mydoc/403-TLS_and_self-signed_certificates.md>
   Advice on Hosting <pages/mydoc/88-Advice_Hosting.md>


Organization Security
=====================

.. toctree::
   :maxdepth: 2

   Safe Travel Recommendations <pages/mydoc/181-Safe_Travel_Recommendations.md>
   Lightweight Security Assessment <pages/mydoc/200-Lightweight_Security_Assessment.md>
   Digital Security Advice when Travelling to China <pages/mydoc/248-Travels_China.md>
   Guiding Questions for High-Risk Organisations <pages/mydoc/262-Guiding_Questions_High-Risk_Orgs.md>
   Organizational Security Policy <pages/mydoc/370-Organizational_Security_Policy.md>

Phishing and Suspicious Email
=============================

.. toctree::
   :maxdepth: 2

   Analysing Suspicious PDF Files <pages/mydoc/363-Analysing_Suspicious_PDF_files.md>
   MS Office Files static analysis <pages/mydoc/394-MS_Office_Files_static_analysis.md>
   Client Receives a Suspicious/Phishing Email <pages/mydoc/58-Suspicious_Phishing_Email.md>

Secure Communications
=====================

.. toctree::
   :maxdepth: 2

   PGP - Transfer Key to New Machine <pages/mydoc/>
   PGP - Lost Keys <pages/mydoc/>
   PGP - GnuPG V2 Warning on Linux Systems <pages/mydoc/>
   Secure Chat Tools for mobile devices <pages/mydoc/>
   PGP - Revoking old key from key servers <pages/mydoc/>
   Recommendations on Encrypted Email Web Apps <pages/mydoc/>
   PGP - Add UserID <pages/mydoc/>
   Install XMPP+OTR on Linux, Windows and macOS <pages/mydoc/>
   Set-up Instructions for XMPP and OTR on Linux, Windows and macOS <pages/mydoc/>
   PGP Key Signing <pages/mydoc/>
   Secure Email Recommendations <pages/mydoc/>
   Secure Survey Tools <pages/mydoc/>
   Remove PGP Passphrase from Cache <pages/mydoc/>
   PGP - Extend Expiration Date <pages/mydoc/>
   WebRTC - recommendations and troubleshooting <pages/mydoc/>
   PGP - Issues with Key Transfer <pages/mydoc/>
   Recommendations on VoIP/video Chat Tools <pages/mydoc/>
   GPGTools Issues with Encrypting or Signing <pages/mydoc/>

Vulnerabilities and Malware
===========================

.. toctree::
   :maxdepth: 2

   How to Clean a Malware-Infected Windows Machine <pages/mydoc/>
   Removing Adware from a Windows Machine <pages/mydoc/>
   Report and Disable Malicious C&C Server <pages/mydoc/>
   Advanced Threats Triage Workflow <pages/mydoc/>
   How to Recognize Spear-Phishing and What to Do <pages/mydoc/>
   Analysing Suspicious PDF Files <pages/mydoc/>
   MS Office Files static analysis <pages/mydoc/>
   Hardening VMs for static malware analysis <pages/mydoc/>

Web Vulnerabilities
===================

.. toctree::
   :maxdepth: 2

   Website Down <pages/mydoc/>

Email Templates
===============

Account Recovery Templates
==========================

.. toctree::
   :maxdepth: 2

Censorship Templates
====================

.. toctree::
   :maxdepth: 2

Devices and Data Security Templates
===================================

.. toctree::
   :maxdepth: 2

Fake Domain Mitigation Templates
================================

.. toctree::
   :maxdepth: 2

Harassment Templates
====================

.. toctree::
   :maxdepth: 2

Helpline Procedures Templates
=============================

.. toctree::
   :maxdepth: 2

Infrastructure Templates
========================

.. toctree::
   :maxdepth: 2

Organization Security Templates
===============================

.. toctree::
   :maxdepth: 2

Phishing and Suspicious Email Templates
=======================================

.. toctree::
   :maxdepth: 2

Secure Communications Templates
===============================

.. toctree::
   :maxdepth: 2

Vulnerabilities and Malware Templates
=====================================

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
